#include <nRF24L01.h>
#include <SPI.h>
#include <printf.h>
#include <RF24_config.h>
#include <RF24.h>

//Radio setup.
RF24 radio(5,7);
const byte address[6] = "00001";
int localXAxis, localYAxis;
int radioDeadZoneAmount = 10; //this will controll how much dead zone there is on each side. -1000 to 1000
int localDeadZoneAmount = 10;

//used for locking out local input.
bool lockLocalControls = false;

//Motor Pinout.
int leftMotorThrottle = 10;
int leftMotorDirection = 11;

int rightMotorThrottle = 12;
int rightMotorDirection = 13;

//Motor Config settings.
int motorSteering = 0;
int motorMinSpeed = 20;
int motorMaxSpeed = 100;

//Used for turning a momentary button into a toggle button.
int previousState = 0;
unsigned long time = 0;
unsigned long debounce = 200UL;

void setup(){
  //radio.
  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
}

void loop(){
  int dataPacket[3];
  bool TempLocalLockOut = false;

  //checks if radio message is available.
  if (radio.available()) {
    radio.read(&dataPacket, sizeof(dataPacket));

    dataPacket[0];
    dataPacket[1];

    //Check deadzone for radio controller. If controller outsize deadzone then local joystick input is lockedout.
    if(dataPacket[0] > radioDeadZoneAmount && dataPacket[0] < -radioDeadZoneAmount || dataPacket[1] > radioDeadZoneAmount && dataPacket[1] < -radioDeadZoneAmount){
      TempLocalLockOut = true;
    }

    //read Thumb stick down. thumb stick down toggles local input lockout.
    if(dataPacket[2] == 0){
      ToggleLockoutState(dataPacket[2]);
    }
  }
  //if there is no radio input then we need to make sure that the vehicle controls don't get locked out.
  else{
    lockLocalControls = false;
  }

  int leftMotorSpeed = 0;
  int leftMotorSteering = 0;

  int rightMotorSpeed = 0;
  int rightMotorSteering = 0;

  //Use controls from the radio message.
  if(TempLocalLockOut || lockLocalControls){
    //check for deadzoning.
    if(dataPacket[0] < radioDeadZoneAmount && dataPacket[0] > -radioDeadZoneAmount || dataPacket[1] < radioDeadZoneAmount && dataPacket[1] > -radioDeadZoneAmount){
      //do nothing.
      //send 0s to the motors.
    }
    else{
      //set forward & reverse. yAxis
      if(dataPacket[1] > radioDeadZoneAmount || dataPacket[1] < -radioDeadZoneAmount){
        leftMotorSpeed = dataPacket[1];
        rightMotorSpeed = dataPacket[1];
      }
      //add left & right input to that forward & reverse to allow for dynamic turing. xAxis
      if(dataPacket[0] > radioDeadZoneAmount || dataPacket[0] < -radioDeadZoneAmount){
        ///////////////////////I might have to adjust this when you connect motors togeather.///////////////////////////////////////////// SWAP THE - & +
        leftMotorSpeed += (dataPacket[0]*.25);
        rightMotorSpeed -= (dataPacket[0]*.25);
      }
      //ZERO POINT TURN. xAxis.
      if(dataPacket[0] > radioDeadZoneAmount || dataPacket[0] < -radioDeadZoneAmount && dataPacket[1] < radioDeadZoneAmount || dataPacket[1] > -radioDeadZoneAmount){
        ///////////////////////I might have to adjust this when you connect motors togeather.///////////////////////////////////////////// SWAP THE - & +
        leftMotorSpeed = dataPacket[0];
        rightMotorSpeed = -dataPacket[0];
      }
    }
  }

  //Use Local controls.
  else{
    localXAxis = analogRead(0); ///////////////////THESE PINOUTS WILL NEED TO CHANGE WHEN I CONNECT EVERYTHING TOGEATHER/////////////////////////
    localYAxis = analogRead(1); ///////////////////THESE PINOUTS WILL NEED TO CHANGE WHEN I CONNECT EVERYTHING TOGEATHER/////////////////////////

    localXAxis = (localXAxis);
    localYAxis = (localYAxis);

    //set forward & reverse. yAxis
    if(localYAxis > localDeadZoneAmount || localYAxis < -localDeadZoneAmount){
      leftMotorSpeed = localYAxis;
      rightMotorSpeed = localYAxis;
    }
    //add left & right input to that forward & reverse to allow for dynamic turing. xAxis
    if(localXAxis > localDeadZoneAmount || localXAxis < -localDeadZoneAmount){
      ///////////////////////I might have to adjust this when you connect motors togeather.///////////////////////////////////////////// SWAP THE - & +
      leftMotorSpeed += (localXAxis*.25);
      rightMotorSpeed -= (localXAxis*.25);
    }
    //ZERO POINT TURN. xAxis.
    if(localXAxis > localDeadZoneAmount || localXAxis < -localDeadZoneAmount && localYAxis < localDeadZoneAmount || localYAxis > -localDeadZoneAmount){
      ///////////////////////I might have to adjust this when you connect motors togeather.///////////////////////////////////////////// SWAP THE - & +
      leftMotorSpeed = localXAxis;
      rightMotorSpeed = -localXAxis;
    }
  }

  //clamp the motors speeds.
  leftMotorSpeed = constrain(leftMotorSpeed, motorMinSpeed, motorMaxSpeed);
  rightMotorSpeed = constrain(rightMotorSpeed, motorMinSpeed, motorMaxSpeed);

  //Fix the direction of motors.
  //left motor.
  if(leftMotorSpeed > 0){
    //forward
    leftMotorSteering = 0;
  }
  else if(leftMotorSpeed < 0){
    //backward
    leftMotorSteering = 1;
  }

  //right motor.
  if(rightMotorSpeed > 0){
    //forward
    rightMotorSteering = 0;
  }
  else if(rightMotorSpeed < 0){
    //backward
    rightMotorSteering = 1;
  }

  SendLeft(leftMotorSteering, leftMotorSpeed);
  SendRight(rightMotorSteering, rightMotorSpeed);
}

//Toggle local control lockout state.
void ToggleLockoutState(int currentState){
  if(currentState == 0 && previousState == 0 && millis() - time > debounce){
    if(lockLocalControls == true){
      lockLocalControls = false;
    }
    else{
      lockLocalControls = true;
    }
  }
}

//PWM for ESC
void SendLeft(int forward, int speed)
{
  analogWrite(leftMotorThrottle, speed);
  if(forward == 0){
    digitalWrite(leftMotorDirection, LOW);
  }
  else{
    digitalWrite(leftMotorDirection, HIGH);
  }
}

void SendRight(int forward, int speed)
{
  analogWrite(rightMotorThrottle, speed);
  if(forward == 0){
    digitalWrite(rightMotorDirection, LOW);
  }
  else{
    digitalWrite(rightMotorDirection, HIGH);
  }
}